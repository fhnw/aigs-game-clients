module ch.fhnw.richards {
	requires javafx.controls;

	requires java.net.http;
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.datatype.jsr310;
    exports ch.fhnw.richards.aigs_game_clients;
    exports ch.fhnw.richards.TicTacToe_Client;
    exports ch.fhnw.richards.TicTacToe_Client.model to com.fasterxml.jackson.databind;
}
