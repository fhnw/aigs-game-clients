package ch.fhnw.richards.TicTacToe_Client.controller;

import ch.fhnw.richards.TicTacToe_Client.model.Game;
import ch.fhnw.richards.TicTacToe_Client.model.TTT_Model;
import ch.fhnw.richards.TicTacToe_Client.view.TTT_View;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

public class GameController {
	private final TTT_View view;
	private final TTT_Model model;

	public GameController(TTT_Model model, TTT_View view) {
		this.view = view;
		this.model = model;

		// When the game-object changes, it is our turn
		model.getGameProperty().addListener((obs, old, newValue) -> {
			Platform.runLater(() -> {
				if (newValue != null) {
					Game game = (Game) newValue;
					view.gameBoard.updateGame(game);
					if (game.getResult() == true) {
						view.lblResult.setText("Game Over!");
						view.toolsSetup.setDisable(false);
						view.gameBoard.setDisable(true);
					}
				}
			});
		});

		// Attach EventHandlers to the game buttons
		for (int col = 0; col < 3; col++) {
			for (int row = 0; row < 3; row++) {
				view.gameBoard.getButtons()[row][col].setOnAction(this::userMove);
			}
		}
	}

	private void userMove(ActionEvent e) {
		Button btn = (Button) e.getSource();
		int col = 0;
		int row = 0;
		boolean found = false;
		while (col < 3 && !found) {
			row = 0;
			while (row < 3 && !found) {
				found = (btn == view.gameBoard.getButtons()[row][col]);
				if (!found)
					row++;
			}
			if (!found)
				col++;
		}
		btn.setText("X");
		model.move(row, col);
	}
}
