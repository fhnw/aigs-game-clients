package ch.fhnw.richards.TicTacToe_Client.controller;

import ch.fhnw.richards.TicTacToe_Client.model.TTT_Model;
import ch.fhnw.richards.TicTacToe_Client.view.TTT_View;
import javafx.application.Platform;
import javafx.event.ActionEvent;

public class SetupController {
	private final TTT_View view;
	private final TTT_Model model;

	public SetupController(TTT_Model model, TTT_View view) {
		this.view = view;
		this.model = model;

		view.toolsSetup.btnStart.setOnAction(this::start);
	}

	private void start(ActionEvent e) {
		Platform.runLater(() -> {
			view.toolsSetup.setDisable(true);
			view.gameBoard.setDisable(false);
		});
		String options = ""; // Currently no options available
		long difficulty = (long) view.toolsSetup.sliderDifficulty.getValue();
		model.newGame(options, difficulty);
	}
}
