package ch.fhnw.richards.TicTacToe_Client.controller;

import ch.fhnw.richards.TicTacToe_Client.model.TTT_Model;
import ch.fhnw.richards.TicTacToe_Client.view.TTT_View;

public class TTT_Controller {

	public TTT_Controller(TTT_Model model, TTT_View view) {
		new ServerController(model, view);
		new LoginController(model, view);
		new SetupController(model, view);
		new GameController(model, view);
	}	
}
