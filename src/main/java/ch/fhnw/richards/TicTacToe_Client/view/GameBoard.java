package ch.fhnw.richards.TicTacToe_Client.view;

import ch.fhnw.richards.TicTacToe_Client.model.Game;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class GameBoard extends GridPane {
	public final Button[][] buttons = new Button[3][3];

	public GameBoard() {
		super();

		// Create the playing board: buttons in 3 columns & 3 rows
		for (int col = 0; col < 3; col++) {
			for (int row = 0; row < 3; row++) {
				// Create
				Button btn = new Button();
				btn.getStyleClass().add("gameButton");

				// Add to layout
				this.add(btn, col, row);

				// Add to array of buttons, for easy reference
				buttons[row][col] = btn;

				// Format
				btn.setPrefSize(120, 120);
			}
		}
		this.setDisable(true);
		this.getStyleClass().add("gameBoard");
	}
	
	public void updateGame(Game game) {
		long[][] board = game.getBoard();
		
		for (int col = 0; col < 3; col++) {
			for (int row = 0; row < 3; row++) {
				if (board[row][col] > 0) buttons[row][col].setText("X");
				else if (board[row][col] < 0) buttons[row][col].setText("O");
				else buttons[row][col].setText(" ");
				
				buttons[row][col].setDisable(board[row][col] != 0 || game.getResult() == true);
			}
		}
	}
	
	public Button[][] getButtons() {
		return buttons;
	}
}
