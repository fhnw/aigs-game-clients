package ch.fhnw.richards.TicTacToe_Client.view;

import ch.fhnw.richards.TicTacToe_Client.model.TTT_Model;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TTT_View {
	private final TTT_Model model;
	
	public final Stage stage;
	// Create the top toolbars
	public final ServerToolBar toolsServer = new ServerToolBar();
	public final LoginToolBar toolsLogin = new LoginToolBar();
	public final SetupToolBar toolsSetup = new SetupToolBar();	
	public final GameBoard gameBoard = new GameBoard();
	public final Label lblResult = new Label();
	
	public TTT_View(Stage stage, TTT_Model model) {
		this.model = model;
		this.stage = stage;

		// Create the root layout
		VBox root  = new VBox(toolsServer, toolsLogin, toolsSetup, gameBoard, lblResult);
		root.getStyleClass().add("vbox");
		
		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("TicTacToe.css").toExternalForm());
		stage.setTitle("Tic-Tac-Toe");
		stage.setScene(scene);	
	}
	
	
	public void show() {
		stage.show();
	}
}

