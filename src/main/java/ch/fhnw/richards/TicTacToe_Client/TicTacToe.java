package ch.fhnw.richards.TicTacToe_Client;

import ch.fhnw.richards.TicTacToe_Client.controller.TTT_Controller;
import ch.fhnw.richards.TicTacToe_Client.model.TTT_Model;
import ch.fhnw.richards.TicTacToe_Client.view.TTT_View;
import javafx.application.Application;
import javafx.stage.Stage;

public class TicTacToe extends Application {
	private TTT_Model model;
	private TTT_View view;
	private TTT_Controller controller;
	
	public static void main(String[] args) {
		launch();
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		model = new TTT_Model();
		view = new TTT_View(stage, model);
		controller = new TTT_Controller(model, view);
		view.show();
	}

}
