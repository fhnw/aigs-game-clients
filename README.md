# AIGS Game Clients

This project contains one or more sample clients for the AIGS server. Refer to the server documentation for more information.

## License

This is open source software, licensed under the BSD 3-clause license.
